### Passerelle externe ###

#!/bin/bash

### Tout Remettre à zéro
iptables -t nat -F
iptables -F
### Accepter les messages ICMP en entrée
iptables -A INPUT -p ICMP -j ACCEPT
### Tout accepter sur l'interface locale
iptables -A INPUT -i lo -j ACCEPT
### Suivi de connexions (stateful firewall)
# Sur les deux chaînes :
# - INPUT concernant la passerelle
# - FORWARD concernant les paquets routés
iptables -A INPUT   -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
### Règles de filtrage
### Quelques variables


DMZ='192.168.1.0/24'
BASTION_EXT='10.10.41.121'
BASTION_INT='192.168.1.1'
LAN='172.16.1.0/24'
IF_EXTERNE='eth0'
IF_INTERNE='eth1'

## — La passerelle d'abord
iptables -A INPUT -i $IF_EXTERNE -d $BASTION_EXT -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -i $IF_INTERNE -s $DMZ -d $BASTION_INT -p tcp --dport 3128 -j ACCEPT
iptables -A INPUT -i $IF_INTERNE -s $LAN -d $BASTION_INT -p udp --dport 3128 -j ACCEPT
iptables -A INPUT -i $IF_INTERNE -s $DMZ -d $BASTION_INT -p tcp --dport 53 -j ACCEPT
iptables -A INPUT -i $IF_INTERNE -s $LAN -d $BASTION_INT -p udp --dport 53 -j ACCEPT
## — Puis les flux



### Déni explicite par défaut
iptables -A INPUT -i $IF_INTERNE -j REJECT
iptables -A INPUT -i $IF_EXTERNE -j DROP
iptables -A FORWARD -i $IF_INTERNE -j REJECT
iptables -A FORWARD -i $IF_EXTERNE -j DROP
