* Matrice de flux après TP 7

* Matrice des flux externe :

| Source ↓ / Destination → |     DMZ     | Passerelle externe | Internet |
|--------------------------+-------------+--------------------+----------|
| DMZ                      |      -      |   dns et proxy     | Interdit |
| Passerelle externe       |ssh seulement|         -          | Autorisé |
| Internet                 |  Interdit   |   ssh seulement    |    -     |

* Matrice des flux interne :

| Source ↓ / Destination → | Réseau local | Passerelle interne |   DMZ     |
|--------------------------+--------------+--------------------+-----------|
| Réseau local             |      -       |    Interdit        |proxy & DNS|     
| Passerelle interne       |    Autorisé  |        -           |proxy & DNS|
| DMZ                      |   Interdit   |      Interdit      |      -    |
